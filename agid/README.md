# AGID testing with jMeter  

## How to retrieve jMeter  

You'll need to retrieve jmeter and launch it. -> <https://jmeter.apache.org/download_jmeter.cgi>  
Personally I retrieved the source and extracted it, then you can start it by launching jmeter in the bin folder.  

## How to use it  

Currently available AGIs scenarios :  

- Testing check_schedule AGI  
- Testing monitoring AGI  

### How to point the load execution toward your virtual environment  

You'll need to customize the TCP Sample Config with your XiVO IP and AGID port.  
You'll also need to open AGID on the external by editing /etc/xivo-agid/config.yml (I personally set 0.0.0.0 because it's an internal lab)  
You can also specify a greater pool size in this config file by adding AGID-specific key `connection_pool_size`. It is at 10 by default, so adding more than 10 thread will refuse requests.  
Finally, in the same config file, you'd better set debug to true to have an easier time to see activity in logs.  

### How to customize the load power  

By editing the Thread Group on each scenario, you should be able to tweak the number of threads (representing users), the ramp up period (to have progressive speed in requesting the service) and the loop count by thread (number of request per user).  

### How to execute the scenario and see some results  

Click start (the green play button on top), there should be a display of the current threads working on top right corner of the application. Then, when the execution is finished, you can check the "Result Tree" component for some feedbacks on potential failures or request blocked, etc ...  

### How to create your own scenarios  

You can start with the template jmx file, it will include the default structure to do an agi call. You will need to edit the TCP Sampler in the Thread Group to match your agi and eventually add other TCP Sampler to create a dialogue. Follow the previous informations in this readme to customize your test.  

### Traps / Tips  

- The EOL byte value must be set to 10 on the TCP Sampler Config.  
- You can use contains instead of match on the Response Assertion to prevent linebreak issues and stuff.  
- You will need to have one or two empty lines after each TCP message in your samplers to indicate the end of the message.  
- There are a couple of checkboxes available in the Thread Group, the most useful ones are the ones that set behavior on error (break, continue ...) and the one to use same user on each iteration.  
- There are also a couple of checkboxes available in each TCP Sampler, the most useful ones are the one to re-use connection in case of a dialogue scenario with multiples back and forth, and the one to close the connection on the last TCP Sampler of your scenario.  
- And there are also some interesting options in the response assertion that allow you to specify what you are expecting in the responses, you can specify some headers to check or check the status response, etc... 
- If you need to retrieve the TCP dialogue of a specific non-included AGI to create your scenario and add it, the best way is to use the asterisk agi debug.   
