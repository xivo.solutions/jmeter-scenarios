# jMeter scenarios repository

### What is it

The repository to store and retrieve jMeter scenarios to test our components. Feel free to add your own stuff or edit those that are already pushed to improve them.  

There is only AGID for now, with a more detailed README in it's own folder.
